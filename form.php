<html>
  <head>
  	 <meta name="lera" content="text/html: charset=utf-8">
  	 <title>Back4</title>
     <meta name="lera" content="text/html: charset=utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
  	 
    <style>
        #error {
             border: 2px solid red;
        }
        
        #content {
             width: 500px;
             padding: 15px;
             background-color: rgb(158, 216, 218);
             font-weight: 500;
        }
        #mychek {
             margin: 25px;
        }
    </style>
  </head>
  <body>

<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>
	<div class="container" id="content">
    	<form action="" method="POST">
    		<div class="form-group">
                <label>Name:</label>
    			<input type="text" name="fio" class="form-control" <?php if ($errors['fio']) {print 'id="error"';} ?> value="<?php print $values['fio']; ?>" />
      		</div>
      		<div class="form-group">
                <label for="email">Email:</label>
                <input type="text" name="email" class="form-control" <?php if ($errors['email']) {print 'id="error"';} ?> value="<?php print $values['email']; ?>" />
      		</div>
      		<div class="form-group">
                <label>DBirth:</label>
                
                <select class="form-control" name="year" <?php if ($errors['year']) {print 'id="error"';} ?> >
                	<?php
                	$select=array(1998=>'', 1999 => '',2000 => '',2001 => '',2002 => '',2003 => '');
                	for($s=1998; $s<=2003; $s++){
                        if($values['year']==$s){
                                    $select[$s]='selected';
                                }
                            }
                     ?>
    				<?php for($i = 1998; $i < 2003; $i++) { ?>
    				<option value="<?php print $i; ?>" <?php print $select[$i]?> ><?= $i; ?></option>
    				<?php } ?>
  				</select>
            </div>
      		<span <?php if ($errors['year']) {print 'id="error"';} ?> >Sex:</span>
            <br>
            <div class="form-check-inline">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="sex" value="female" <?php if($values['sex']=='female') {print'checked';}?>>female
                </label>
            </div>
            <div class="form-check-inline">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="sex" value="male" <?php if($values['sex']=='male') {print'checked';}?>>male
                </label>
            </div>
      		<div class="form-group">
                Number of limbs <br>
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="kon" value="0" <?php if($values['kon']=='0') {print'checked';}?>>0
                    </label>
                </div>
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="kon" value="1" <?php if($values['kon']=='1') {print'checked';}?>>1
                    </label>
                </div>
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="kon" value="2" <?php if($values['kon']=='2') {print'checked';}?>>2
                    </label>
                </div>
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="kon" value="3" <?php if($values['kon']=='3') {print'checked';}?>>3
                    </label>
                </div>
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="kon" value="4" <?php if($values['kon']=='4') {print'checked';}?>>4
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label>Abilities:</label>
                
                <select class="form-control" name="abilities[]" multiple <?php if ($errors['abilities']) {print 'id="error"';} ?>>
                <?php 
                foreach ($abilities as $key => $value) {
                    $selected = empty($values['abilities'][$key]) ? '' : 'selected="selected"';
                    printf('<option value="%s" %s>%s</option>', $key, $selected, $value);
                } ?>
                </select>
            <div class="form-group">
                <label for="comment">biography</label>
                <textarea class="form-control" rows="5" name="bio" <?php if ($errors['bio']) {print 'id="error"';} ?>><?php print $values['bio']; ?></textarea>
            </div>
            <label class="form-check-label" id="mychek"><input class="form-check-input" type="checkbox" name="check" <?php if (!$errors['check']) {print 'checked';} else {print 'id="error"';} ?>> I'm agree </label>
      		<input type="submit" value="ok" />
    	</form>
    </div>
  </body>
</html>
